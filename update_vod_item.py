import boto3
import os
import time
import math

dynamodb = boto3.resource('dynamodb', region_name='us-east-1')

bucket_name = os.environ['bucket_name']
file_name = os.environ['file_name']
folder_id = os.environ['video_id']
cloudfront_address = os.environ['cloudfront_address']

vod_table = dynamodb.Table(os.environ['DYNAMODB_TABLE'])

print('@@@@@@ RAW_VIDEO_DURATION @@@@@@')
print(os.environ['RAW_VIDEO_DURATION'])

raw_video_duration = float(os.environ['RAW_VIDEO_DURATION'])

if int(raw_video_duration) < 60:
    minutes = 0
    seconds = int(raw_video_duration)
else:
    minutes = int(math.modf(raw_video_duration)[1] / 60)
    seconds = math.modf(raw_video_duration / 60)[0] * 60

timestamp = int(time.time() * 1000)

result = vod_table.update_item(
    Key={
        'id': os.environ['video_id']
    },
    ExpressionAttributeValues={
        ':video_url': 'https://' + cloudfront_address + '/' + folder_id + '/hls' + '/playlist.m3u8',
        ':poster_url': 'https://' + cloudfront_address + '/' + folder_id + '/images' + '/poster.jpg',
        ':poster_480_url': 'https://' + cloudfront_address + '/' + folder_id + '/images' + '/poster480.jpg',
        ':poster_320_url': 'https://' + cloudfront_address + '/' + folder_id + '/images' + '/poster320.jpg',
        ':poster_120_url': 'https://' + cloudfront_address + '/' + folder_id + '/images' + '/poster120.jpg',
        ':poster_40_url': 'https://' + cloudfront_address + '/' + folder_id + '/images' + '/poster40.jpg',
        ':status': 'Ready', 
        ':durationMinutes': int(minutes),
        ':durationSeconds': int(seconds),
        ':updatedAt': timestamp,
    },
    UpdateExpression='SET videoStatus = :status, videoUrl = :video_url, posterUrl = :poster_url, poster480Url = '
                     ':poster_480_url, poster320Url = :poster_320_url, poster120Url = :poster_120_url, poster40Url = '
                     ':poster_40_url,  durationMinutes = :durationMinutes, durationSeconds = :durationSeconds, updatedAt = :updatedAt',
    ReturnValues='ALL_NEW',
)
