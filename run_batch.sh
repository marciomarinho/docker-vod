#!/bin/bash

echo $DYNAMODB_TABLE
echo $bucket_key 
echo $file_name 
echo $cloudfront_address
echo $bucket_name
echo $video_id
echo $delivery_bucket_name

aws s3 cp s3://$bucket_name/$bucket_key .

ls -la

mkdir files
mkdir files/$video_id
mkdir files/$video_id
mkdir files/$video_id/hls
mkdir files/$video_id/images

raw_video_duration=`./ffprobe -v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 ${file_name}`
export RAW_VIDEO_DURATION=${raw_video_duration}

./ffmpeg -hide_banner -y -i $file_name \
  -vf scale=w=640:h=360:force_original_aspect_ratio=decrease -c:a aac -ar 48000 -c:v h264 -profile:v main -crf 20 -sc_threshold 0 -g 48 -keyint_min 48 -hls_time 4 -hls_playlist_type vod  -b:v 800k -maxrate 856k -bufsize 1200k -b:a 96k -hls_segment_filename files/$video_id/hls/360p_%03d.ts files/$video_id/hls/360p.m3u8 \
  -vf scale=w=842:h=480:force_original_aspect_ratio=decrease -c:a aac -ar 48000 -c:v h264 -profile:v main -crf 20 -sc_threshold 0 -g 48 -keyint_min 48 -hls_time 4 -hls_playlist_type vod -b:v 1400k -maxrate 1498k -bufsize 2100k -b:a 128k -hls_segment_filename files/$video_id/hls/480p_%03d.ts files/$video_id/hls/480p.m3u8 \
  -vf scale=w=1280:h=720:force_original_aspect_ratio=decrease -c:a aac -ar 48000 -c:v h264 -profile:v main -crf 20 -sc_threshold 0 -g 48 -keyint_min 48 -hls_time 4 -hls_playlist_type vod -b:v 2800k -maxrate 2996k -bufsize 4200k -b:a 128k -hls_segment_filename files/$video_id/hls/720p_%03d.ts files/$video_id/hls/720p.m3u8 \
  -vf scale=w=1920:h=1080:force_original_aspect_ratio=decrease -c:a aac -ar 48000 -c:v h264 -profile:v main -crf 20 -sc_threshold 0 -g 48 -keyint_min 48 -hls_time 4 -hls_playlist_type vod -b:v 5000k -maxrate 5350k -bufsize 7500k -b:a 192k -hls_segment_filename files/$video_id/hls/1080p_%03d.ts files/$video_id/hls/1080p.m3u8

cp playlist.m3u8 files/$video_id/hls/playlist.m3u8

# generate thumbnails
./ffmpeg -itsoffset -1 -i $file_name -vframes 1 -filter:v scale="720:-1" files/$video_id/images/poster.jpg
./ffmpeg -itsoffset -1 -i $file_name -vframes 1 -filter:v scale="480:-1" files/$video_id/images/poster480.jpg
./ffmpeg -itsoffset -1 -i $file_name -vframes 1 -filter:v scale="320:-1" files/$video_id/images/poster320.jpg
./ffmpeg -itsoffset -1 -i $file_name -vframes 1 -filter:v scale="120:-1" files/$video_id/images/poster120.jpg
./ffmpeg -itsoffset -1 -i $file_name -vframes 1 -filter:v scale="40:-1" files/$video_id/images/poster40.jpg

#upload to s3 target bucket
aws s3 sync files s3://${delivery_bucket_name}

python3 update_vod_item.py
