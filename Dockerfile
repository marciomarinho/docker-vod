FROM amazonlinux:2.0.20191016.0

RUN amazon-linux-extras install python3

WORKDIR /root
COPY . /root

# Install any needed packages specified in ./requirements.txt
# RUN pip install --trusted-host pypi.python.org -r requirements.txt

RUN pip3 install boto3
RUN pip3 install awscli

CMD [ "/bin/bash", "/root/run_batch.sh", "start" ]